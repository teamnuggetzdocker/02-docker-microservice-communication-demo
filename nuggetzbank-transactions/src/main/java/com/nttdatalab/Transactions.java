package com.nttdatalab;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("transactions")
public class Transactions {

    @GET
    public String transaction() {
        return "a transaction response";
    }
}
